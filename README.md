To start app you need to have docker and docker-compose.

To start you need to execute command `make init`. After composer installation and migration applying you can see app on `http://localhost:8000/`.
