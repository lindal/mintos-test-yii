<?php


namespace app\models\user;


use yii\base\Model;

class RegistryForm extends Model
{
    public $email;
    public $password;

    public function rules()
    {
        return [
            [['email', 'password'], 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => User::class, 'targetAttribute' => 'email'],
            ['password', 'string']
        ];
    }

    public function registry(): bool
    {
        if (!$this->validate()) {
            return false;
        }
        $model = new User([
            'email' => $this->email,
            'password_hash' => \Yii::$app->security->generatePasswordHash($this->password)
        ]);
        if (!$model->save()) {
            \Yii::error($model->getErrors());
            throw new \Exception("Can't save user");
        }
        return true;
    }
}