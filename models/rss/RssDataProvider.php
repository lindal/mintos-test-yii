<?php


namespace app\models\rss;


use app\models\rss\source\SourceInterface;

class RssDataProvider
{

    /** @var SourceInterface */
    private $source;
    private $data = [];

    public function __construct(SourceInterface $source)
    {
        $this->source = $source;
    }

    /**
     * @param int $limit
     * @return Item[]
     */
    public function getFeed(int $limit = 0): array
    {
        if (!$this->data) {
            $this->loadData();
        }
        if ($limit <= 0) {
            return $this->data;
        }
        return array_slice($this->data, 0, $limit);
    }

    private function loadData(): void
    {
        foreach ($this->source->getFeed() as $row) {
            $item = new Item();
            $item->id = $row['id'];
            $item->updated = $row['updated'];
            $item->authorName = $row['authorName'];
            $item->authorURI = $row['authorURI'];
            $item->link = $row['link'];
            $item->title = $row['title'];
            $item->summary = $row['summary'];
            $this->data[] = $item;
        }
    }

}