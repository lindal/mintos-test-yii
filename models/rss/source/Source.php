<?php


namespace app\models\rss\source;

class Source implements SourceInterface
{

    private $feedURL;

    public function __construct(string $feedURL)
    {
        $this->feedURL = $feedURL;
    }

    /**
     * @inheritDoc
     */
    public function getFeed(): array
    {
        $items = [];
        $feed = simplexml_load_file($this->feedURL);
        foreach ($feed->entry ?? [] as $item) {
            $items[] = [
                'id' => (string)$item->id,
                'updated' => (string)$item->updated,
                'authorName' => (string)$item->author->name,
                'authorURI' => (string)$item->author->uri,
                'link' => (string)$item->link['href'],
                'title' => (string)$item->title,
                'summary' => (string)$item->summary,
            ];
        }
        return $items;
    }

}