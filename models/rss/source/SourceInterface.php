<?php


namespace app\models\rss\source;


use app\models\rss\Item;

interface SourceInterface
{

    /**
     * @return array
     */
    public function getFeed(): array;

}