<?php


namespace app\models\rss;


class Item
{

    public $id;
    public $updated;
    public $authorName;
    public $authorURI;
    public $link;
    public $title;
    public $summary;

}