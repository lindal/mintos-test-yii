<?php


namespace app\models\rss\analyzer;


interface CounterInterface
{

    /**
     * @param string[] $data
     * @return array
     */
    public function count(array $data): array;

}