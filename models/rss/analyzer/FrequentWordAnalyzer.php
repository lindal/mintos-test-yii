<?php


namespace app\models\rss\analyzer;


class FrequentWordAnalyzer
{

    /** @var SeparatorInterface */
    private $separator;
    /** @var HandlerInterface[] */
    private $handlers = [];
    /** @var CounterInterface */
    private $counter;

    const LIMIT = 10;

    /**
     * @param Item[] $items
     * @return array
     */
    public function getTop(array $items): array
    {
        foreach ($items as $item) {
            $strings[] = strip_tags($item->title . ' ' . $item->summary);
        }
        $result = $this->separator->separate(implode('. ', $strings));
        foreach ($this->handlers as $handler) {
            $result = $handler->apply($result);
        }
        $result = $this->counter->count($result);
        arsort($result);
        return array_slice($result, 0, self::LIMIT);
    }

    /**
     * @param SeparatorInterface $separator
     * @return FrequentWordAnalyzer
     */
    public function setSeparator(SeparatorInterface $separator): FrequentWordAnalyzer
    {
        $this->separator = $separator;
        return $this;
    }

    /**
     * @param CounterInterface $counter
     * @return FrequentWordAnalyzer
     */
    public function setCounter(CounterInterface $counter): FrequentWordAnalyzer
    {
        $this->counter = $counter;
        return $this;
    }

    /**
     * @param HandlerInterface $handler
     * @return FrequentWordAnalyzer
     */
    public function addHandlers(HandlerInterface $handler): FrequentWordAnalyzer
    {
        $this->handlers[] = $handler;
        return $this;
    }

}