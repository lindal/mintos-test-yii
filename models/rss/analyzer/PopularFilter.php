<?php

namespace app\models\rss\analyzer;


class PopularFilter implements HandlerInterface
{

    private const MOST_POPULAR = [
        'the',
        'be',
        'to',
        'of',
        'and',
        'a',
        'in',
        'that',
        'have',
        'i',
        'it',
        'for',
        'not',
        'on',
        'with',
        'he',
        'as',
        'you',
        'do',
        'at',
        'this',
        'but',
        'his',
        'by',
        'from',
        'they',
        'we',
        'say',
        'her',
        'she',
        'or',
        'an',
        'will',
        'my',
        'one',
        'all',
        'would',
        'there',
        'their',
        'what',
        'so',
        'up',
        'out',
        'if',
        'about',
        'who',
        'get',
        'which',
        'go',
        'me',
    ];

    /**
     * @inheritDoc
     */
    public function apply(array $data): array
    {
        return array_diff($data, self::MOST_POPULAR);
    }

}