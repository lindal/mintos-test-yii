<?php


namespace app\models\rss\analyzer;


class Counter implements CounterInterface
{

    /**
     * @inheritDoc
     */
    public function count(array $data): array
    {
        $result = [];
        foreach ($data as $item) {
            if (!isset($result[$item])) {
                $result[$item] = 1;
                continue;
            }
            $result[$item]++;
        }
        return $result;
    }
}