<?php


namespace app\models\rss\analyzer;


interface HandlerInterface
{

    /**
     * @param string[] $data
     * @return string[]
     */
    public function apply(array $data): array;

}