<?php


namespace app\models\rss\analyzer;


class ShortNormalizer implements HandlerInterface
{
    private const DATA = [
        'im' => ['i', 'am'],
        'id' => ['i', 'had', 'would', 'should'],
        'ill' => ['i', 'will'],
        'ive' => ['i', 'have'],
        'youre' => ['you', 'are'],
        'youd' => ['you', 'had', 'would'],
        'youll' => ['you', 'will'],
        'youve' => ['you', 'have'],
        'hes' => ['he', 'is', 'has'],
        'hed' => ['he', 'had', 'would'],
        'hell' => ['he', 'will'],
        'shes' => ['she', 'is', 'has'],
        'shed' => ['she', 'had', 'would'],
        'shell' => ['she', 'will'],
        'its' => ['it', 'is', 'has'],
        'itll' => ['it', 'will'],
        'were' => ['we', 'are'],
        'wed' => ['we', 'had', 'should', 'would'],
        'well' => ['we', 'will'],
        'weve' => ['we', 'have'],
        'theyre' => ['they', 'are'],
        'theyd' => ['they', 'had', 'would'],
        'theyll' => ['they', 'will'],
        'theyve' => ['they', 'have'],
        'theres' => ['there', 'is', 'has'],
        'therell' => ['there', 'will'],
        'thered' => ['there', 'had', 'would'],
        'isnt' => ['is', 'not'],
        'arent' => ['are', 'not'],
        'dont' => ['do', 'not'],
        'doesnt' => ['does', 'not'],
        'wasnt' => ['was', 'not'],
        'werent' => ['were', 'not'],
        'didnt' => ['did', 'not'],
        'havent' => ['have', 'not'],
        'hasnt' => ['has', 'not'],
        'wont' => ['will', 'not'],
        'hadnt' => ['had', 'not'],
        'cant' => ['cannot'],
        'couldnt' => ['could', 'not'],
        'mustnt' => ['must', 'not'],
        'mightnt' => ['might', 'not'],
        'neednt' => ['need', 'not'],
        'shouldnt' => ['should', 'not'],
        'oughtnt' => ['ought', 'not'],
        'wouldnt' => ['would', 'not'],
        'whats' => ['what', 'is'],
        'hows' => ['how', 'is'],
        'wheres' => ['where', 'is']
    ];

    /**
     * @inheritDoc
     */
    public function apply(array $data): array
    {
        $result = [];
        foreach ($data as $item) {
            $replaced = preg_replace('/\W+/', '', $item);
            if (!isset(self::DATA[$replaced])) {
                $result[] = $item;
                continue;
            }
            foreach (self::DATA[$replaced] as $word) {
                $result[] = $word;
            }
        }
        return $result;
    }
}