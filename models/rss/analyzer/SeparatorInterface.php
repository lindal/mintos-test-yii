<?php


namespace app\models\rss\analyzer;


interface SeparatorInterface
{

    /**
     * @param string $data
     * @return string[]
     */
    public function separate(string $data): array;

}