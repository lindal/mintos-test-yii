<?php


namespace app\models\rss\analyzer;


class WordSeparator implements SeparatorInterface
{

    /**
     * @inheritDoc
     */
    public function separate(string $text): array
    {
        preg_match_all("/\w+`?'?\w?/s", $text, $match);
        return array_values(array_filter(array_map(function ($item) {
            return is_numeric($item) ? null : trim(strtolower($item));
        }, $match[0] ?? [])));
    }

}