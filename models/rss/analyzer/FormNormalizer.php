<?php


namespace app\models\rss\analyzer;


class FormNormalizer implements HandlerInterface
{

    private const DATA = [
        'is' => 'be',
        'are' => 'be',
        'has' => 'have',
        'was' => 'be',
        'were' => 'be',
        'had' => 'have'
        //... and more
    ];

    /**
     * @inheritDoc
     */
    public function apply(array $data): array
    {
        return array_map(function ($item) {
            return self::DATA[$item] ?? $item;
        }, $data);
    }
}