run:
	docker-compose stop
	docker-compose up -d

init: run
	docker-compose exec php composer install
	docker-compose exec php php yii migrate --interactive=0

