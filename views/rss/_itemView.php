<?php
/**
 * @var $model \app\models\rss\Item
 */
?>
<div>
    <p>Author: <?= $model->authorName ?></p>
    <p><?= $model->title ?></p>
    <p><?= strip_tags($model->summary) ?></p>
    <?= \yii\helpers\Html::a($model->link, $model->link, ['target' => '_blank']) ?>
</div>
