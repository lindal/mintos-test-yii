<?php

/* @var $this \yii\web\View */
/* @var $rssProvider \app\models\rss\RssDataProvider */
/* @var $analyzer \app\models\rss\analyzer\FrequentWordAnalyzer */
$items = $rssProvider->getFeed();
$dataProvider = new \yii\data\ArrayDataProvider([
    'allModels' => $items,
    'pagination' => [
        'pageSize' => 20
    ]
])
?>

<div class="top-words">
    <h2>10 most frequent words</h2>
    <ul>
        <?php
        foreach ($analyzer->getTop($items) as $word => $count) {
            echo \yii\helpers\Html::tag("li", "{$word}: {$count}");
        }
        ?>
    </ul>
</div>

<div class="rss-feed">
    <h2>Feed</h2>
    <?= \yii\widgets\ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_itemView',
        'separator' => '<hr/>'
    ])?>
</div>