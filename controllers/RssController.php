<?php


namespace app\controllers;


use app\models\rss\analyzer\Counter;
use app\models\rss\analyzer\FormNormalizer;
use app\models\rss\analyzer\FrequentWordAnalyzer;
use app\models\rss\analyzer\PopularFilter;
use app\models\rss\analyzer\ShortNormalizer;
use app\models\rss\analyzer\WordSeparator;
use app\models\rss\RssDataProvider;
use app\models\rss\source\Source;
use yii\filters\AccessControl;
use yii\web\Controller;

class RssController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    public function actionIndex()
    {
        $rssProvider = new RssDataProvider(new Source(\Yii::$app->params['rss_url']));
        $analyzer = new FrequentWordAnalyzer();
        $analyzer->setSeparator(new WordSeparator())
            ->setCounter(new Counter())
            ->addHandlers(new ShortNormalizer())
            ->addHandlers(new FormNormalizer())
            ->addHandlers(new PopularFilter());

        return $this->render('index', [
            'rssProvider' => $rssProvider,
            'analyzer' => $analyzer
        ]);
    }


}