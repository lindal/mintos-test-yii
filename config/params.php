<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'rss_url' => 'https://www.theregister.co.uk/software/headlines.atom'
];
